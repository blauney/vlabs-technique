let service;
let map;
let markers = [];
const maps = {
    init() {
        // load map
        maps.loadMap();

        let places = document.querySelector('#google_maps').getAttribute('data-addresses');
        let addresses = [];
        JSON.parse(places).forEach((place) => {
            addresses.push({place: place.address})
        })

        addresses.map((address, i) => {
            maps.getPlace(addresses[i % addresses.length].place)
        })

        // see all points on the map
        document.querySelector('.all').addEventListener('click', () => {
            // location of all addresses with a marker
            addresses.map((address, i) => {
                maps.getPlace(addresses[i % addresses.length].place, 6)
            })
        })

        document.querySelectorAll('.place').forEach((value) => {
            value.addEventListener('click', (e) => {
                const $this = e.currentTarget
                const idPoint = $this.getAttribute('data-id');
                const url = $this.getAttribute('data-link')

                // remove previous marker
                maps.setMapOnAll(null);

                fetch(url.replace('ZZZ', idPoint))
                    .then((response) => {
                        return response.json();
                    })
                    .then((data) => {
                        this.getPlace(data.results[0].formatted_address, 15)
                    })
            })
        })
    },
    loadMap() {
        map = new google.maps.Map(document.querySelector('#google_maps'), {
            zoom: 6,
            center: {lat: 47.100, lng: 1.800}
        });
    },
    getPlace(address, zoom) {
        const request = {
            query: address,
            fields: ["name", "geometry", 'formatted_address'],
        };

        let service = new google.maps.places.PlacesService(map);
        service.findPlaceFromQuery(request, (results, status) => {
            if (status === google.maps.places.PlacesServiceStatus.OK && results) {
                for (let i = 0; i < results.length; i++) {
                    maps.createMarker(results[i]);
                }
                map.setCenter(results[0].geometry.location);
                if (zoom != ''){
                    map.setZoom(zoom);
                }
            }
        });
    },
    createMarker(place) {
        if (!place.geometry || !place.geometry.location) return;

        const marker = new google.maps.Marker({
            map,
            position: place.geometry.location,
            animation: google.maps.Animation.DROP,
            title: place.formatted_address,
        });

        markers.push(marker)
    },
    setMapOnAll(map) {
        for (let i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {
    maps.init();
})