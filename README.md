# V-Labs Test Technique

Créer et liste des points d'interets

## Développement
### Configuration

- Copier le fichier __.env__ en __.env.local__ et configurer la base de donnée avec les infos suivant :
```
POSTGRES_HOST: nom_du_container -> database
POSTGRES_DB: vlabs_technique
POSTGRES_USER: symfony
POSTGRES_PASSWORD: symfony
```
- Copier le fichier __.env.test__ en __.env.test.local__ et configurer la base de donnée
- Lancer les commandes suivantes : 
```
- make build-php-apache -> image php-apache (php8.1)
- make build -> image de l'appli
- make database -> création des tables
- make up -> up de tous les container nécéssaire
```

### Création User
- Exécuter le fichier sql ```sql/user.sql```
- Login : 
```
Email : devAdmin@admin.com
Password : admin
```

### TEST UNITAIRE
- Lancer la commande ```make database-test``` afin de créer la base de donnée de test
- Puis la commande ```make test``` pour lancer les tests unitaires