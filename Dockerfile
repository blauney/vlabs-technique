FROM devweb/php8.1-apache

USER root

#INSTALL NODE lts
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
RUN apt-get install -y nodejs

#INSTALL NPM
RUN curl -qL https://www.npmjs.com/install.sh | sh

# INSTALL YARN
RUN npm install -g yarn

COPY . /var/www/vlabs-technique

COPY .docker/vhost/vlabs-technique.local.conf /etc/apache2/sites-available/

RUN cd /etc/apache2/sites-available
RUN a2ensite vlabs-technique.local.conf
RUN service apache2 restart

USER devweb

WORKDIR /var/www/vlabs-technique