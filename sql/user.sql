INSERT INTO public."user" (id, email, roles, password, firstname, lastname)
VALUES (1, 'devAdmin@admin.com', '["ROLE_SUPER_ADMIN"]', '$2y$13$vo7ybFWgr8GIWb2mjBTvguEFSXrycjpcC0spmgqqYWS/UvO9yXtE2', 'admin', 'root');

create database vlabs_technique_test
    with owner symfony;

-- create table
create table point_of_interest
(
    id      integer      not null
        primary key,
    name    varchar(255) not null,
    address varchar(255) not null
);

alter table point_of_interest
    owner to symfony;

create sequence point_of_interest_id_seq;

alter sequence point_of_interest_id_seq owner to symfony;