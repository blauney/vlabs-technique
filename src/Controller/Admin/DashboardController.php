<?php

namespace App\Controller\Admin;

use App\Entity\PointOfInterest;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    public function __construct(private AdminUrlGenerator $adminUrlGenerator)
    {
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
//        return parent::index();

        $url = $this->adminUrlGenerator
            ->setController(PointOfInterestCrudController::class)
            ->generateUrl();

        return $this->redirect($url);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Vlabs Test Technique');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToRoute('Voir la carte', 'fa fa-map', 'homepage');

        yield MenuItem::section('Point d\'interet', 'fa fa-map-marker');
        yield MenuItem::subMenu('Actions', 'fa fa-bars')->setSubItems([
           MenuItem::linkToCrud('Ajouter un lieu', 'fa fa-plus', PointOfInterest::class)
               ->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Voir tous les lieux', 'fa fa-eye', PointOfInterest::class)
        ]);
    }
}
