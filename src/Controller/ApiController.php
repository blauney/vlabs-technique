<?php

namespace App\Controller;

use App\Entity\PointOfInterest;
use App\Repository\PointOfInterestRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api')]
class ApiController extends AbstractController
{
    #[Route('/create-point', name: 'api_create_point', methods: "POST")]
    public function createPoint(Request $request, EntityManagerInterface $entityManager): Response
    {
        $pointOfInterest = new PointOfInterest();
        $errors = [];

        if (!empty($request->get('name'))) {
            $pointOfInterest->setName($request->get('name'));
        } else {
            $errors['message'][] = 'Veuillez renseigner un nom';
        }

        if (!empty($request->get('address'))) {
            $pointOfInterest->setAddress($request->get('address'));
        } else {
            $errors['message'][] = 'Veuillez renseigner une adresse';
        }

        if (!empty($errors)) {
            $errors['status_code'] = Response::HTTP_UNPROCESSABLE_ENTITY;
            return $this->json($errors, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $entityManager->persist($pointOfInterest);
        $entityManager->flush();

        $headers = [
            "Accept: application/json",
            "Content-Type: application/json",
        ];

        return $this->json([], Response::HTTP_CREATED, $headers);
    }

    #[Route('/points-of-interests', name: 'api_list_point_of_interest', methods: "GET")]
    public function getPointOfInterest(Request $request, PointOfInterestRepository $pointOfInterestRepository): Response
    {
        $pointOfInterest = $pointOfInterestRepository->findAll();

        if (!empty($request->query->all())) {
            $pointOfInterest = $pointOfInterestRepository->findOneByName($request->get('name'));
        }

        if (null === $pointOfInterest) {
            $errors = [
                'message' => 'Aucun Point d\'interet trouvé',
                'status_code' => Response::HTTP_NOT_FOUND
            ];
            return $this->json($errors, Response::HTTP_NOT_FOUND);
        }

        return $this->json($pointOfInterest);
    }
}
