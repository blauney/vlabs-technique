include ./docker.env
.PHONY: webapp container
all: build up

build-php-apache:
	docker build -t ${NAMESPACE}/php8.1-apache .docker/php-apache --build-arg RUNTIME_PHP_VERSION=8.1
build:
	docker-compose -f docker-compose.yml build
up:
	docker-compose -f docker-compose.yml up -d
down:
	docker-compose -f docker-compose.yml down
vendor:
	docker exec -it vlabs-technique composer install
	docker exec -it vlabs-technique yarn install
migration:
	docker exec -it vlabs-technique php bin/console doctrine:migrations:migrate
test:
	docker exec -it vlabs-technique ./vendor/bin/phpunit
database-test:
	docker exec -it vlabs-technique php bin/console doctrine:database:create --env=test
	docker exec -it vlabs-technique php bin/console doctrine:migrations:migrate --env=test