<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/user')]
class UserController extends AbstractController
{
    #[Route('/', name: 'user_index')]
    public function index(UserRepository $userRepository): JsonResponse
    {
        return $this->json($userRepository->findAll());
    }

    #[Route('/register', name: 'user_register')]
    public function register(UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $entityManager): Response
    {
        $user = new User();
        $user->setEmail('devAdmin@admin.com')
            ->setPassword($passwordHasher->hashPassword($user, 'admin'))
            ->setRoles(['ROLE_SUPER_ADMIN'])
            ->setFirstname('admin')
            ->setLastname('root');

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->render('user/register.html.twig');
    }

    #[Route('/{id}', name: 'user_show')]
    public function show(User $user, UserRepository $userRepository): JsonResponse
    {
        return $this->json($userRepository->findOneById($user->getId()));
    }
}
