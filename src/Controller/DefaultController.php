<?php

namespace App\Controller;

use App\Repository\PointOfInterestRepository;
use App\Service\GoogleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function index(PointOfInterestRepository $pointOfInterestRepository): Response
    {
        $points = $pointOfInterestRepository->findAll();

        return $this->render('default/index.html.twig', [
            'points' => $pointOfInterestRepository->findAll(),
            'addresses' => $this->json($points)
        ]);
    }

    #[Route('/point-of-interest/{id}', name: 'point_of_interest_infos')]
    public function getInfosByAddress(
        int $id,
        PointOfInterestRepository $pointOfInterestRepository,
        GoogleService $googleService
    ) {
        $point = $pointOfInterestRepository->findOneById($id);

        try {
            $infos = $googleService->getInfosPlace($point->getAddress());

            return $this->json($infos);
        } catch (ClientExceptionInterface $clientException) {
            return $this->json($clientException->getResponse()->getContent(false));
        }
    }
}
