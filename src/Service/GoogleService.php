<?php
declare(strict_types=1);

namespace App\Service;


use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GoogleService
{
    private const API_KEY = 'AIzaSyDNmwdasFOXQLkTbdu3iK_zOnwL-9-CHEY';
    private const URL_API = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=';

    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->client = $httpClient;
    }


    /**
     * @param string $address
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getInfosPlace(string $address): array
    {
        $response = $this->client->request('GET', self::URL_API . $address . '&sensor=false&key=' . self::API_KEY);
        return $response->toArray();
    }
}