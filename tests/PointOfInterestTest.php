<?php

namespace App\Tests;

use App\Repository\PointOfInterestRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class PointOfInterestTest extends WebTestCase
{
    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();

        self::bootKernel();
    }

    public function testCreatePoint()
    {
        $this->client->request('POST', 'api/create-point', [
            'name' => 'Bordeaux',
            'address' => '33000 Bordeaux'
        ]);

        $this->assertEquals($this->client->getResponse()->getStatusCode(), Response::HTTP_CREATED);
    }

    public function testGetPointOfInterest()
    {
        $this->client->request('GET', 'api/points-of-interests?name=Bordeaux');

        $this->assertEquals($this->client->getResponse()->getStatusCode(), Response::HTTP_OK);
    }
}
